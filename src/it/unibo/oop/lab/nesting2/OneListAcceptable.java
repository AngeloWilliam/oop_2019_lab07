package it.unibo.oop.lab.nesting2;

import java.util.LinkedList;
import java.util.List;

/**
 * Class rappresents a list that implements acceptable behavior.
 * 
 * @param <T>
 */
public class OneListAcceptable<T> implements Acceptable<T>{
	
	private List<T> list;
	private int stop;
	
	public OneListAcceptable(final List<T> list){
		this.list = new LinkedList<T>(list);
		this.stop = this.list.size();
	}
	
	/**
	 * {@inheritDoc}
	 */
	public Acceptor<T> acceptor() {
		return this.new Acceptorn();
	}
	
	/**
	 * Class implements Acceptor behavior.
	 */
	private class Acceptorn implements Acceptor<T>{
		
		/**
		 * Rappresents the position of element controlled.
		 */
		private int current;
		
		public Acceptorn() {
			
		}
		
		/**
		 * {@inheritDoc}
		 */
		public void accept(T newElement) throws ElementNotAcceptedException {
			if(this.current <OneListAcceptable.this.stop && OneListAcceptable.this.list.get(this.current) == newElement) {
				current++;
			}
			else {
				throw new ElementNotAcceptedException(newElement);
			}
		}

		/**
		 * {@inheritDoc}
		 */
		public void end() throws EndNotAcceptedException {
			if(this.current < OneListAcceptable.this.stop) {
				throw new EndNotAcceptedException();
			}
		}
		
	}

}
