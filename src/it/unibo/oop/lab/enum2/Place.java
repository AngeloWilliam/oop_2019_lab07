package it.unibo.oop.lab.enum2;

/**
 * Represents an enumeration for declaring Place;
 * 
 */
public enum Place {
	INDOOR, OUTDOOR
}
