/**
 * 
 */
package it.unibo.oop.lab.enum2;

/**
 * Represents an enumeration for declaring sports.
 * 
 * 1) You must add a field keeping track of the number of members each team is
 * composed of (1 for individual sports)
 * 
 * 2) A second field will keep track of the name of the sport.
 * 
 * 3) A third field, of type Place, will allow to define if the sport is
 * practiced indoor or outdoor
 * 
 */
public enum Sport {
	
	BASKET(Place.OUTDOOR,"basket",5),
	SOCCER(Place.OUTDOOR,"soccer",11),
	TENNIS(Place.INDOOR,"tennis",1),
	BIKE(Place.OUTDOOR,"bike",1),
	F1(Place.OUTDOOR,"f1",1),
	MOTOGP(Place.OUTDOOR,"motogp",1),
	VOLLEY(Place.INDOOR,"volley",5);

	private Place place;
	private String actualName;
	private int noTeamMembers;

	Sport(){}
	
	/**
	 * Builds new sport, i.e. a sport with place (INDOOR or OUTDOOR), name, number of team's members passed. 
	 * @param place
	 * @param actualName
	 * @param noTeamMembers
	 */
	Sport(final Place place, final String actualName, final int noTeamMembers){
		this.place = place;
		this.actualName = actualName;
		this.noTeamMembers = noTeamMembers;
	}
	
    /**
     * Returns true if the sport on which the method is called is an individual sport
     * 
     * @return true if this is an individual sport 	.
     */
	public boolean isIndividualSport() {
		return this.noTeamMembers == 1;
	}
	
	
	/**
	 * Returns true if the sport on which the method is called is an indoor sport
	 * 
	 * @return true if this is an indoor sport.
	 */
	public boolean isIndoorSport() {
		return this.place == Place.INDOOR;
	}
	
    /**
     * Returns place (indoor or outdoor) where sport is practiced
     * 
     * @return the place where sport is practiced.
     */
	public Place getPlace() {
		return this.place;
	}
	
	/**
	 * Returns a String that described this sport: sport's name, place 
	 * place where it is practised,number of team's members.
	 * 
	 * @return a string that described this sport
	 */
	public String toString() {
		return this.actualName + " is a " + this.place + " and it played by " 
				+ this.noTeamMembers + (this.noTeamMembers==1 ? " member" : " members");
	}
}
